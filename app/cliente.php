<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
   protected $fillable = [
     'Nombres',
     'Apellidos',
     'Cedula',
     'Direccion',
     'Telefono',
     'Fecha_nacimiento',
     'email'

   ];
}
