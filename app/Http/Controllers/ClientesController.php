<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class ClientesController extends Controller
{

    public function InicioCliente(Request $request)
    {
        $cliente = cliente::all();
       return view('clientes.inicio')->with('cliente', $cliente);
    }

    public function CrearCliente(Request $request)
    {
        $cliente = cliente::all();
        return view('clientes.crear')->with('cliente', $cliente);
    }
    
    public function GuardarCliente(Request $request){
        $this->validate($request, [
            'Nombres' => 'required',
            'Apellidos'=> 'required',
            'Cedula'=> 'required',
            'Direccion'=> 'required',
            'Telefono'=> 'required',
            'Fecha_nacimiento'=> 'required',
            'email'=> 'required'
        ]);

        $cliente = new cliente;
        $cliente->Nombres=$request->Nombres;
        $cliente->Apellidos=$request->Apellidos;
        $cliente->Cedula=$request->Cedula;
        $cliente->Direccion=$request->Direccion;
        $cliente->Telefono=$request->Telefono;
        $cliente->Fecha_nacimiento=$request->Fecha_nacimiento;
        $cliente->email=$request->email;
        $cliente->save();
        return redirect()->route('list.clientes');
    }
}