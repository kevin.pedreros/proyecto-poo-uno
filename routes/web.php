<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){

    Route::get('/lista/clientes',['as' => 'list.clientes', 'uses' =>'ClientesController@InicioCliente']);
Route::get('/crear/clientes',['as' => 'crear.clientes', 'uses' =>'ClientesController@CrearCliente']);
Route::post('/guardar/clientes',['as' => 'guardar.clientes', 'uses' =>'ClientesController@GuardarCliente']);

});